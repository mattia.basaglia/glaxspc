PYTHON=python3
HERE=$(dir $(abspath $(lastword $(MAKEFILE_LIST))))
ENVNAME=env
PROJECT_NAME=glaxspc

.PHONY: test coverage env pip_install


test:
	$(PYTHON) -m unittest discover -t "$(HERE)" -s test

coverage:
	coverage erase
	coverage run --branch --source="$(HERE)/glaxspc" -m unittest discover -t "$(HERE)" -s test
	coverage html
	coverage report --skip-covered
	echo "file://$(HERE)/htmlcov/index.html"

env:
	virtualenv -p $(PYTHON) --prompt "($(PROJECT_NAME)) " $(join $(HERE), $(ENVNAME))


pip_install:
	pip install -r $(join $(HERE), "requirements.txt")
