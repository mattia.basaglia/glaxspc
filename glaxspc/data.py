import os
import io
import bisect
import tarfile
import fnmatch
from contextlib import contextmanager


class AbstractLoader:
    """!
    @breif Base class for data file loaders
    """

    def open(self, filename: str, binary: bool):
        """!
        @brief Returns a context manager for the matching file or none
        """
        if self._can_open(filename):
            return self._on_open(filename, binary)
        return None

    def _on_open(self, filename: str, binary: bool):
        raise NotImplementedError

    def _can_open(self, filename: str):
        return False

    def list(self, pattern: str):
        """!
        @brief returns a set of matching file names
        """
        return set()

    def _match_pattern(self, pattern: str, path: str):
        return fnmatch.fnmatch(path, pattern)

    def _priority_index(self):
        """!
        @brief Loader primary priority, larger numbers are selected before lower ones
        """
        return 0

    def _priority_name(self):
        """!
        @brief Loader secondary priority, lexicographically larger strings are selected before lower ones
        """
        return ""

    def _priority(self):
        return (self._priority_index(), self._priority_name())

    def __lt__(self, other: "AbstractLoader"):
        return self._priority() > other._priority()


class FileSystemLoader(AbstractLoader):
    """!
    @brief Loader for files in the filesystem
    """
    def __init__(self, path):
        self.path = path

    def _can_open(self, filename):
        return os.path.exists(os.path.join(self.path, filename))

    def _on_open(self, filename, binary: bool):
        return open(os.path.join(self.path, filename), "rb" if binary else "r")

    def _list_collect(self, pattern, path):
        found = set()
        for info in os.scandir(path):
            if info.is_dir():
                found |= self._list_collect(pattern, info.path)
            else:
                relpath = os.path.relpath(info.path, self.path)
                if self._match_pattern(pattern, relpath):
                    found.add(relpath)

        return found

    def list(self, pattern: str):
        return self._list_collect(pattern, self.path)

    def _priority_index(self):
        return 500

    def _priority_name(self):
        return ""


class TarballLoader(AbstractLoader):
    """!
    @brief Loader for files in a tape archive
    """
    def __init__(self, path):
        self._path = path
        self._tar = None
        self._pn = os.path.splitext(os.path.basename(path))[0]
        if self._pn.endswith(".tar"):
            self._pn = self._pn[:-4]

    def _load(self):
        if self._tar is None and os.path.exists(self._path):
            self._tar = tarfile.open(self._path)
        return self._tar

    def open(self, filename: str, binary: bool):
        if not self._load():
            return None

        try:
            info = self._tar.getmember(filename)
        except KeyError:
            return None

        if not info.isfile():
            return None

        file = self._tar.extractfile(info)
        if binary:
            return file
        return io.TextIOWrapper(file)

    def list(self, pattern: str):
        if not self._load():
            return set()
        return set(
            path
            for path in self._tar.getnames()
            if self._match_pattern(pattern, path)
        )

    def _priority_index(self):
        return 100

    def _priority_name(self):
        return self._pn


class Loader(AbstractLoader):
    """!
    @brief Loader selecting files based on other loader
    """
    def __init__(self):
        self.loaders = []

    def add_loader(self, loader: AbstractLoader):
        """!
        @brief Adds a loader
        """
        bisect.insort(self.loaders, loader)

    def open(self, filename: str, binary: bool):
        for loader in self.loaders:
            file = loader.open(filename, binary)
            if file:
                return file

        return None

    def list(self, pattern: str):
        matches = set()
        for loader in self.loaders:
            matches |= loader.list(pattern)

        return matches

    def _priority_index(self):
        raise ValueError()
