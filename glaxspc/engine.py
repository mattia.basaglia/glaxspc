import os
import sys
from . import data


class Engine:
    def __init__(self):
        self._paths = []
        self._loader = data.Loader()
        self.write_path = os.getcwd()
        self._games = {}
        self._archives = set()

    def add_path(self, path: str):
        self._paths.append(path)
        self._loader.add_loader(data.FileSystemLoader(path))

    def add_archive(self, path: str):
        self._loader.add_loader(data.TarballLoader(path))

    def load_archives(self, path: str = "data"):
        for search_path in self._paths:
            if os.path.is_dir(search_path):
                for file in os.scandir(os.path.join(search_path, path)):
                    if file.is_file() and file.path not in self._archives:
                        self.add_archive(file.path)

    def open_asset(self, path: str, binary: bool):
        file = self._loader.open(str, binary)
        if file is None:
            raise KeyError(path)

        return file

    def open_user(self, path: str, mode: str):
        if "w" in mode:
            os.makedirs(self.write_path, exist_ok=True)

        return open(os.path.join(self.write_path, path))

    def load_game_list(self, pattern: str = "games/*.json"):
        from .game import Game
        for avail in self._loader.list(pattern):
            try:
                game = Game.from_file(self._loader.open(avail, False))
                self._games[game.id] = game
            except Exception as e:
                self.log("Could not load %s" % avail)

    def log(self, message):
        sys.stderr.write(str(message))
        sys.stderr.write("\n")

    def load_game(self, id):
        return self._games[id]
