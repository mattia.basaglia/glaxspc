import sys
import inspect
from .loadable import Loadable


type = {
    "string": str,
    "number": float,
    "boolean": bool,
}


def cast(self, value, type):
    if type is None:
        return value

    try:
        return type(value)
    except ValueError:
        return default(type)


def default(type):
    if type is str:
        return ""
    if type is float:
        return 0
    if type is bool:
        return False
    return ""


class Variable(Loadable):
    def __init__(self, name, type, initial):
        self.name = name
        self.type = type[type]
        self.initial = initial


class AstNode:
    def evaluate(self, game):
        raise NotImplementedError()


class VariableReference(AstNode):
    def __init__(self, name):
        self.name = name

    def evaulate(self, game):
        return game.state[self.name]


class PythonExpression(AstNode):
    def __init__(self, expr):
        self.expr = expr

    def evaluate(self, game):
        return game.evaluate(self.expr)


class Literal(AstNode):
    def __init__(self, value):
        self.value = value

    def evaluate(self, game):
        return self.value


def _from_json(value, engine):
    if isinstance(value, dict):

        cls = getattr(sys.modules[__name__], value.pop("type", None), None)
        if not inspect.isclass(cls) and not issubclass(cls, AstNode):
            raise TypeError("Class %s not found" % cls)

        kwargs = {}
        for k, v in value.items():
            kwargs[k] = from_json(v)

        return cls(**kwargs)

    else:

        return Literal(value)


def expression_from_json(value, engine) -> AstNode:
    try:
        return _from_json(value, engine)
    except Exception as e:
        engine.log(e)
