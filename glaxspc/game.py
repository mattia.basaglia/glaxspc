from .loadable import Loadable
from .expressions import expression_from_json
from .engine import Engine


class Game(Loadable):
    def __init__(self, engine: Engine, id, version, initial_scene, init_files):
        self.engine = engine
        self.id = id
        self.initial_scene = initial_scene
        self.scene = None
        self.version = version
        self.images = {}
        self.variables = {}
        self.state = {}
        self.scenes = {}
        self.init_files = init_files

    def _globals(self):
        return {"game": self}

    def evaluate(self, code):
        return eval(code, self._globals(), self.state)

    def execute(self, code):
        exec(code, self._globals(), self.state)


class ImageAsset:
    def __init__(self, data):
        self.data = data


class ImageReference(Loadable):
    def __init__(self, engine, image, flipped):
        self.image = image
        self.flipped = flipped


class Character(Loadable):
    def __init__(self, engine, name, images):
        self.name = expression_from_json(name, engine)
        self.images = ImageReference.load_many_dict(engine, images)


class ActionMetaclass(type):
    _registered = {}

    def __new__(cls, name, bases, namespace):
        abstract = namespace.pop("abstract", False)
        derived = type.__new__(cls, name, bases, namespace)
        if not abstract:
            ActionMetaclass._registered[name] = derived
        return derived


class Action(Loadable, metaclass=ActionMetaclass):
    abstract = True

    @classmethod
    def from_dict(cls, engine, obj_dict) -> Action:
        type = obj_dict.pop("type")
        return ActionMetaclass._registered[type](engine, obj_dict)

    def trigger(self, game: Game):
        raise NotImplementedError()


class ActionDialog(Action):
    def __init__(self, engine, dialog):
        self.dialog = dialog

    def trigger(self, game: Game):
        game.scene.trigger_dialog(game, self.dialog)


class ActionScene(Action):
    def __init__(self, engine, scene):
        self.scene = scene

    def trigger(self, game: Game):
        game.switch_scene(self.scene)


class ActionUpdateState(Action):
    def __init__(self, engine, variable, value, increase):
        self.variable = variable
        self.value = expression_from_json(value, engine)
        self.increase = increase

    def trigger(self, game: Game):
        value = self.value.evaluate(game)
        if self.increase:
            game.state[self.variable] += self.value
        else:
            game.state[self.variable] = self.value


class RunScript(Action):
    def __init__(self, engine, filename):
        self.filename = filename
        self.code = None

    def trigger(self, game: Game):
        if self.code is None:
            self.code = compile(game.engine.open_asset(self.filename, False), self.filename, "exec")
        game.execute(self.code)


class Dialog(Loadable):
    pass


class Scene(Loadable):
    def __init__(self, engine, background, dialogs, start):
        self.background = background
        self.dialogs = Dialog.load_many_dict(engine, dialogs)
        self.start = Action.from_dict(engine, start)

    def run(self, game: Game):
        self.start.trigger(game)
