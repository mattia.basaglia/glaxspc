import json


class Loadable:
    @classmethod
    def from_dict(cls, engine, obj_dict: dict):
        return cls(engine, **obj_dict)

    @classmethod
    def from_string(cls, engine, string: str):
        return cls.from_dict(engine, json.loads(string))

    @classmethod
    def from_file(cls, engine, file):
        return cls.from_dict(engine, json.load(file))

    @classmethod
    def load_many_dict(cls, engine, obj_dict: dict):
        return {
            k: cls.from_dict(v)
            for k, v in obj_dict.items()
        }

    def to_dict(self):
        raise NotImplementedError()

    def to_file(self, file, **kw):
        json.dump(self.to_dict(), file, **kw)

    def to_string(self, **kw):
        return json.dumps(self.to_dict(), **kw)
