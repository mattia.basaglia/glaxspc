import os
import io
import shutil
import tarfile
import tempfile
from unittest import TestCase, mock
from contextlib import contextmanager

import glaxspc.data


class TestAbstractLoader(TestCase):
    def test_open_context(self):
        contextmag = mock.MagicMock()

        class Derived(glaxspc.data.AbstractLoader):
            can_open = False

            @contextmanager
            def _on_open(self, filename, binary):
                contextmag.open(filename)
                with contextmag:
                    yield contextmag

            def _can_open(self, filename: str):
                return self.can_open

        obj = Derived()

        obj.can_open = False
        self.assertIsNone(obj.open("foobar", False))
        contextmag.open.assert_not_called()
        contextmag.__enter__.assert_not_called()
        contextmag.__exit__.assert_not_called()

        obj.can_open = True
        opened = obj.open("foobar", False)
        self.assertIsNotNone(opened)
        with opened:
            contextmag.open.assert_called_once_with("foobar")
            contextmag.__enter__.assert_called()
            contextmag.__exit__.assert_not_called()
        contextmag.__exit__.assert_called()

    def test_default_raises(self):
        obj = glaxspc.data.AbstractLoader()

        self.assertRaises(NotImplementedError, obj._on_open, "foobar", False)
        self.assertIsNone(obj.open("foobar", False))

    def test_list_unloaded(self):
        loader = glaxspc.data.AbstractLoader()
        self.assertSetEqual(
            loader.list("*/bar"),
            set(),
        )


class TestFileSystemLoader(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.dir = os.path.join(tempfile.gettempdir(), "__glaxspc_test")
        os.makedirs(os.path.join(cls.dir, "foo"))
        with open(os.path.join(cls.dir, "foo", "bar"), "w") as f:
            f.write("baz")

        os.makedirs(os.path.join(cls.dir, "zoo"))
        with open(os.path.join(cls.dir, "zoo", "bar"), "w") as f:
            f.write("zaz")
        with open(os.path.join(cls.dir, "zoo", "baz"), "w") as f:
            f.write("zzz")

        cls.loader = glaxspc.data.FileSystemLoader(cls.dir)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.dir, True)

    def test_open_non_existing(self):
        self.assertIsNone(self.loader.open("bar/foo", False))

    def test_open_binary(self):
        file = self.loader.open("foo/bar", True)
        self.assertIsNotNone(file)

        with file:
            self.assertEqual(file.read(), b"baz")

    def test_open_text(self):
        file = self.loader.open("foo/bar", False)
        self.assertIsNotNone(file)

        with file:
            self.assertEqual(file.read(), "baz")

    def test_cmp(self):
        loader_1 = glaxspc.data.TarballLoader("foo/bar-0001.tar.gz")
        loader_2 = glaxspc.data.TarballLoader("zoo/bar-0012.tar.gz")
        loader_3 = glaxspc.data.TarballLoader("foo/bar-0023.tar.gz")

        self.assertTrue(loader_1 > loader_2)
        self.assertTrue(loader_1 > loader_3)
        self.assertTrue(loader_2 > loader_3)

        self.assertFalse(loader_2 > loader_1)
        self.assertFalse(loader_3 > loader_1)
        self.assertFalse(loader_3 > loader_2)

        self.assertFalse(loader_1 < loader_1)

    def test_list_wild_dir(self):
        self.assertSetEqual(
            self.loader.list("*/bar"),
            {"foo/bar", "zoo/bar"},
        )

    def test_list_wild_part(self):
        self.assertSetEqual(
            self.loader.list("foo/b*r"),
            {"foo/bar"},
        )

    def test_list_wild_name(self):
        self.assertSetEqual(
            self.loader.list("zoo/*"),
            {"zoo/bar", "zoo/baz"},
        )


class TestTarballLoader(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.filename = os.path.join(tempfile.gettempdir(), "__glaxspc_test.tar.gz")
        with tarfile.open(cls.filename, "w") as tf:
            srcfile = io.BytesIO(b"baz")
            info = tarfile.TarInfo(name="foo/bar")
            info.size = len(srcfile.getvalue())
            tf.addfile(tarinfo=info, fileobj=srcfile)

            tf.addfile(tarinfo=tarfile.TarInfo(name="zoo/bar"))
            tf.addfile(tarinfo=tarfile.TarInfo(name="zoo/baz"))

            info = tarfile.TarInfo(name="foo")
            info.type = tarfile.DIRTYPE
            tf.addfile(info)

        cls.loader = glaxspc.data.TarballLoader(cls.filename)

    @classmethod
    def tearDownClass(cls):
        os.unlink(cls.filename)

    def test_open_no_tarball(self):
        loader = glaxspc.data.TarballLoader(os.path.join(tempfile.gettempdir(), "__glaxspc_test_notafile.tar.gz"))
        self.assertIsNone(loader.open("foo/bar", False))

    def test_open_non_existing(self):
        self.assertIsNone(self.loader.open("bar/foo", False))

    def test_dir(self):
        self.assertIsNone(self.loader.open("foo", False))

    def test_open_binary(self):
        file = self.loader.open("foo/bar", True)
        self.assertIsNotNone(file)

        with file:
            self.assertEqual(file.read(), b"baz")

    def test_open_text(self):
        file = self.loader.open("foo/bar", False)
        self.assertIsNotNone(file)

        with file:
            self.assertEqual(file.read(), "baz")

    def test_cmp(self):
        loader_1 = glaxspc.data.TarballLoader("foo-0001.tar.gz")
        loader_2 = glaxspc.data.TarballLoader("foo-0023.tar.gz")

        self.assertTrue(loader_1 > loader_2)
        self.assertFalse(loader_2 > loader_1)
        self.assertFalse(loader_1 > loader_1)

    def test_cmp_ext_different(self):
        loader_1 = glaxspc.data.TarballLoader("foo-0001.tar.gz")
        loader_2 = glaxspc.data.TarballLoader("foo-0002.tar.bz2")
        loader_3 = glaxspc.data.TarballLoader("foo-0003.tar")

        self.assertTrue(loader_1 > loader_2)
        self.assertTrue(loader_1 > loader_3)
        self.assertTrue(loader_2 > loader_3)

        self.assertFalse(loader_1 < loader_2)
        self.assertFalse(loader_1 < loader_3)
        self.assertFalse(loader_2 < loader_3)

    def test_cmp_ext_same(self):
        loader_1 = glaxspc.data.TarballLoader("foo-0001.tar.gz")
        loader_2 = glaxspc.data.TarballLoader("foo-0001.tar.bz2")
        loader_3 = glaxspc.data.TarballLoader("foo-0001.tar")

        self.assertFalse(loader_1 > loader_2)
        self.assertFalse(loader_1 > loader_3)
        self.assertFalse(loader_2 > loader_3)

        self.assertFalse(loader_1 < loader_2)
        self.assertFalse(loader_1 < loader_3)
        self.assertFalse(loader_2 < loader_3)

    def test_list_unloaded(self):
        loader = glaxspc.data.TarballLoader(os.path.join(tempfile.gettempdir(), "__glaxspc_test_notafile.tar.gz"))
        self.assertSetEqual(
            loader.list("*/bar"),
            set(),
        )

    def test_list_wild_dir(self):
        self.assertSetEqual(
            self.loader.list("*/bar"),
            {"foo/bar", "zoo/bar"},
        )

    def test_list_wild_part(self):
        self.assertSetEqual(
            self.loader.list("foo/b*r"),
            {"foo/bar"},
        )

    def test_list_wild_name(self):
        self.assertSetEqual(
            self.loader.list("zoo/*"),
            {"zoo/bar", "zoo/baz"},
        )


class TestLoader(TestCase):
    @classmethod
    def setUpClass(cls):
        class Derived(glaxspc.data.AbstractLoader):
            def __init__(self, name):
                self.name = name
                self.file = mock.MagicMock()

            def _on_open(self, filename, binary):
                self.file(filename, binary)
                return self.file

            def _can_open(self, filename: str):
                return self.name == filename

            def list(self, pattern):
                return {self.name} if self._match_pattern(pattern, self.name) else set()

        cls.loader_class = Derived

    def test_not_found(self):
        loader = glaxspc.data.Loader()
        loader.add_loader(self.loader_class("foo/bar"))
        loader.add_loader(self.loader_class("foo/baz"))
        self.assertIsNone(loader.open("bar/foo", True))

    def test_first(self):
        loader = glaxspc.data.Loader()
        loader.add_loader(self.loader_class("foo/bar"))
        loader.add_loader(self.loader_class("foo/baz"))
        self.assertIs(loader.open("foo/bar", True), loader.loaders[0].file)
        loader.loaders[0].file.assert_called_once_with("foo/bar", True)

    def test_second(self):
        loader = glaxspc.data.Loader()
        loader.add_loader(self.loader_class("foo/bar"))
        loader.add_loader(self.loader_class("foo/baz"))
        self.assertIs(loader.open("foo/baz", True), loader.loaders[1].file)
        loader.loaders[1].file.assert_called_once_with("foo/baz", True)

    def test_cmp(self):
        loader = glaxspc.data.Loader()
        self.assertRaises(Exception, lambda: loader < loader)

        loader_1 = glaxspc.data.FileSystemLoader("foo")
        loader_2 = glaxspc.data.TarballLoader("zoo.tar.gz")

        self.assertTrue(loader_1 < loader_2)
        self.assertFalse(loader_2 < loader_1)

    def test_list_wild_dir(self):
        loader = glaxspc.data.Loader()
        loader.add_loader(self.loader_class("foo/bar"))
        loader.add_loader(self.loader_class("zoo/bar"))
        loader.add_loader(self.loader_class("zoo/baz"))

        self.assertSetEqual(
            loader.list("*/bar"),
            {"foo/bar", "zoo/bar"},
        )

    def test_list_wild_part(self):
        loader = glaxspc.data.Loader()
        loader.add_loader(self.loader_class("foo/bar"))
        loader.add_loader(self.loader_class("zoo/bar"))
        loader.add_loader(self.loader_class("zoo/baz"))

        self.assertSetEqual(
            loader.list("foo/b*r"),
            {"foo/bar"},
        )

    def test_list_wild_name(self):
        loader = glaxspc.data.Loader()
        loader.add_loader(self.loader_class("foo/bar"))
        loader.add_loader(self.loader_class("zoo/bar"))
        loader.add_loader(self.loader_class("zoo/baz"))

        self.assertSetEqual(
            loader.list("zoo/*"),
            {"zoo/bar", "zoo/baz"},
        )
